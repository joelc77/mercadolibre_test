package cerviniojoel.mercadolibretest.models;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

public class Setting implements Parcelable {

    @SerializedName("card_number")
    CardNumber cardNumber;
    Bin bin;
    @SerializedName("security_code")
    SecurityCode securityCode;

    public Setting() {
    }

    public Setting(Parcel in) {
        in.readParcelable(CardNumber.class.getClassLoader());
        in.readParcelable(Bin.class.getClassLoader());
        in.readParcelable(SecurityCode.class.getClassLoader());
    }

    public Setting(CardNumber cardNumber, Bin bin, SecurityCode securityCode) {
        this.cardNumber = cardNumber;
        this.bin = bin;
        this.securityCode = securityCode;
    }

    public CardNumber getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(CardNumber cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Bin getBin() {
        return bin;
    }

    public void setBin(Bin bin) {
        this.bin = bin;
    }

    public SecurityCode getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(SecurityCode securityCode) {
        this.securityCode = securityCode;
    }

    public static final Creator<Setting> CREATOR = new Creator<Setting>() {
        @Override
        public Setting createFromParcel(Parcel in) {
            return new Setting(in);
        }

        @Override
        public Setting[] newArray(int size) {
            return new Setting[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeParcelable(cardNumber, 0);
        out.writeParcelable(bin, 0);
        out.writeParcelable(securityCode, 0);
    }
}



