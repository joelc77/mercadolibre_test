package cerviniojoel.mercadolibretest.models;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

public class SecurityCode implements Parcelable {
    String length;
    @SerializedName("card_location")
    String cardLocation;
    String mode;

    public SecurityCode() {
    }
    public SecurityCode(Parcel in) {
        this.length = in.readString();
        this.cardLocation = in.readString();
        this.mode = in.readString();
    }


    public SecurityCode(String length, String cardLocation, String mode) {
        this.length = length;
        this.cardLocation = cardLocation;
        this.mode = mode;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getCardLocation() {
        return cardLocation;
    }

    public void setCardLocation(String cardLocation) {
        this.cardLocation = cardLocation;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public static final Creator<SecurityCode> CREATOR = new Creator<SecurityCode>() {
        @Override
        public SecurityCode createFromParcel(Parcel in) {
            return new SecurityCode(in);
        }

        @Override
        public SecurityCode[] newArray(int size) {
            return new SecurityCode[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeString(this.length);
        out.writeString(this.cardLocation);
        out.writeString(this.mode);
    }
}