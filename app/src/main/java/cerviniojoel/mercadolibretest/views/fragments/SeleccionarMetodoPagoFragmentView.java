package cerviniojoel.mercadolibretest.views.fragments;

import cerviniojoel.mercadolibretest.models.CardIssuer;
import cerviniojoel.mercadolibretest.models.PaymentMethod;

import java.util.List;

public interface SeleccionarMetodoPagoFragmentView {

    void showResultPaymentMethods(List<PaymentMethod> paymentMethodList);
    void showErrorPaymentMethods(String error);


    void showProgress();

    void hideProgress();
//
//    void OnResponseSuccess(T response);
//
//    void onResponseFailure(String throwable);
}
