package cerviniojoel.mercadolibretest.interactors;

import android.content.Context;
import android.util.Log;
import cerviniojoel.mercadolibretest.R;
import cerviniojoel.mercadolibretest.daos.PaymentMethodDAO;
import cerviniojoel.mercadolibretest.interfaces.ResultListener;
import cerviniojoel.mercadolibretest.models.CardIssuer;
import cerviniojoel.mercadolibretest.models.Installments;
import cerviniojoel.mercadolibretest.models.PayerCost;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

import java.util.ArrayList;
import java.util.List;

public class SeleccionarCuotaFragmentInteractorImpl implements SeleccionarCuotaFragmentInteractor {
    final String LOG_TAG = SeleccionarCuotaFragmentInteractorImpl.class.getSimpleName();
    private Context mContext;

    public SeleccionarCuotaFragmentInteractorImpl(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public Observable<List<Installments>> getInstallments(String publicKey, String paymentMethodId, String amount, String issuerId) {
        PaymentMethodDAO paymentMethodDAO = new PaymentMethodDAO();
        return paymentMethodDAO.getInstallmentsFromServer(publicKey, paymentMethodId, amount, issuerId);
    }

    @Override
    public Observer getObserverInstallments(final ResultListener<List<Installments>> resultListener) {
        return new DisposableObserver<List<Installments>>() {
            @Override
            public void onNext(List<Installments> installmentsList) {
                if(installmentsList != null && installmentsList.size()>0) {
                    resultListener.onSuccess(installmentsList);
                }
                else {
                    resultListener.onError(mContext.getString(R.string.msj_error_generico_banco));
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.e(LOG_TAG, "getObserverInstallments(): " + e.toString());
                resultListener.onError(mContext.getString(R.string.msj_error_generico));
            }

            @Override
            public void onComplete() {

            }
        };
    }

    @Override
    public void getListaCuotas(List<Installments> installmentsList, final ResultListener<List<String>> resultListener) {
        List<String> cuotasLista = new ArrayList<>();

        try {
            if (installmentsList != null && installmentsList.size() > 0) {
                Installments installments = installmentsList.get(0);
                if (installments != null && installments.getPayerCosts() != null && installments.getPayerCosts().size() > 0) {
                    List<PayerCost> payerCostList = installments.getPayerCosts();
                    for (int i = 0; i < payerCostList.size(); i++) {
                        PayerCost payerCost = payerCostList.get(i);
                        if (payerCost != null) {
                            cuotasLista.add(String.valueOf(payerCost.getInstallments()));
                        }
                    }
                }
            }

            if (cuotasLista.size() > 0) {
                resultListener.onSuccess(cuotasLista);
            } else {
                resultListener.onError("Errores");
            }
        } catch (Exception e) {
            resultListener.onError("Errores");
        }


    }

    @Override
    public void getPayerCost(List<Installments> installmentsList, String cuota, ResultListener<PayerCost> resultListener) {
        PayerCost payerCost = null;
        try {
            if (installmentsList != null && installmentsList.size() > 0) {
                Installments installments = installmentsList.get(0);
                if (installments != null && installments.getPayerCosts() != null && installments.getPayerCosts().size() > 0) {
                    List<PayerCost> payerCostList = installments.getPayerCosts();
                    for (int i = 0; i < payerCostList.size(); i++) {
                        if (payerCostList.get(i) != null && payerCostList.get(i).getInstallments() == Integer.parseInt(cuota)) {
                            payerCost = payerCostList.get(i);
                        }
                    }
                }
            }

            if (payerCost != null) {
                resultListener.onSuccess(payerCost);
            } else {
                resultListener.onError("Errores");
            }
        } catch (Exception e) {
            resultListener.onError("Errores");
        }

    }
}
