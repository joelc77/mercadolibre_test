package cerviniojoel.mercadolibretest.models;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PayerCost implements Parcelable {
    int installments;
    @SerializedName("installment_rate")
    float installmentRate;
    @SerializedName("discount_rate")
    int discountRate;
    List<String> labels;
    @SerializedName("installment_rate_collector")
    List<String> installmentRateCollector;
    @SerializedName("min_allowed_amount")
    int minAllowedAmount;
    @SerializedName("max_allowed_amount")
    int maxAllowedAmount;
    @SerializedName("recommended_message")
    String recommendedMessage;
    @SerializedName("installment_amount")
    float installmentAmount;
    @SerializedName("total_amount")
    float totalAmount;

    public PayerCost() {
    }

    public PayerCost(Parcel in) {
        this.installments = in.readInt();
        this.installmentRate = in.readFloat();
        this.discountRate = in.readInt();
        in.readStringList(this.labels);
        in.readStringList(this.installmentRateCollector);
        this.minAllowedAmount = in.readInt();
        this.maxAllowedAmount = in.readInt();
        this.recommendedMessage = in.readString();
        this.installmentAmount = in.readFloat();
        this.totalAmount = in.readFloat();
    }

    public PayerCost(int installments, int installmentRate, int discountRate, List<String> labels, List<String> installmentRateCollector,
                     int min_allowed_amount, int max_allowed_amount, String recommended_message, int installment_amount, int total_amount) {
        this.installments = installments;
        this.installmentRate = installmentRate;
        this.discountRate = discountRate;
        this.labels = labels;
        this.installmentRateCollector = installmentRateCollector;
        this.minAllowedAmount = min_allowed_amount;
        this.maxAllowedAmount = max_allowed_amount;
        this.recommendedMessage = recommended_message;
        this.installmentAmount = installment_amount;
        this.totalAmount = total_amount;
    }

    public int getInstallments() {
        return installments;
    }

    public void setInstallments(int installments) {
        this.installments = installments;
    }

    public float getInstallmentRate() {
        return installmentRate;
    }

    public void setInstallmentRate(float installmentRate) {
        this.installmentRate = installmentRate;
    }

    public int getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(int discountRate) {
        this.discountRate = discountRate;
    }

    public List<String> getLabels() {
        return labels;
    }

    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    public List<String> getInstallmentRateCollector() {
        return installmentRateCollector;
    }

    public void setInstallmentRateCollector(List<String> installmentRateCollector) {
        this.installmentRateCollector = installmentRateCollector;
    }

    public int getMin_allowed_amount() {
        return minAllowedAmount;
    }

    public void setMin_allowed_amount(int min_allowed_amount) {
        this.minAllowedAmount = min_allowed_amount;
    }

    public int getMax_allowed_amount() {
        return maxAllowedAmount;
    }

    public void setMax_allowed_amount(int max_allowed_amount) {
        this.maxAllowedAmount = max_allowed_amount;
    }

    public String getRecommended_message() {
        return recommendedMessage;
    }

    public void setRecommended_message(String recommended_message) {
        this.recommendedMessage = recommended_message;
    }

    public float getInstallment_amount() {
        return installmentAmount;
    }

    public void setInstallment_amount(float installment_amount) {
        this.installmentAmount = installment_amount;
    }

    public float getTotal_amount() {
        return totalAmount;
    }

    public void setTotal_amount(float totalAmount) {
        this.totalAmount = totalAmount;
    }

    public static final Parcelable.Creator<PayerCost> CREATOR = new Parcelable.Creator<PayerCost>() {
        @Override
        public PayerCost createFromParcel(Parcel in) {
            return new PayerCost(in);
        }

        @Override
        public PayerCost[] newArray(int size) {
            return new PayerCost[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeInt(this.installments);
        out.writeFloat(this.installmentRate);
        out.writeInt(this.discountRate);
        out.writeStringList(this.labels);
        out.writeStringList(this.installmentRateCollector);
        out.writeInt(this.minAllowedAmount);
        out.writeInt(this.maxAllowedAmount);
        out.writeString(this.recommendedMessage);
        out.writeFloat(this.installmentAmount);
        out.writeFloat(this.totalAmount);
    }
}