package cerviniojoel.mercadolibretest.interfaces;

import cerviniojoel.mercadolibretest.models.CardIssuer;
import cerviniojoel.mercadolibretest.models.PayerCost;
import cerviniojoel.mercadolibretest.models.PaymentMethod;

public interface SeleccionarCuotaFragmentListener {
    void onCompletePago (String amount, PaymentMethod paymentMethod, CardIssuer cardIssuer, PayerCost payerCost);
}
