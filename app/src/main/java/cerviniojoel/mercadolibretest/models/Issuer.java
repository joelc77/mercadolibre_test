package cerviniojoel.mercadolibretest.models;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

public class Issuer implements Parcelable {
    String id;
    String name;
    @SerializedName("secure_thumbnail")
    String secureThumbnail;
    String thumbnail;

    public Issuer() {
    }

    public Issuer(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.secureThumbnail = in.readString();
        this.thumbnail = in.readString();
    }

    public Issuer(String id, String name, String secureThumbnail, String thumbnail) {
        this.id = id;
        this.name = name;
        this.secureThumbnail = secureThumbnail;
        this.thumbnail = thumbnail;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecureThumbnail() {
        return secureThumbnail;
    }

    public void setSecureThumbnail(String secureThumbnail) {
        this.secureThumbnail = secureThumbnail;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public static final Creator<Issuer> CREATOR = new Creator<Issuer>() {
        @Override
        public Issuer createFromParcel(Parcel in) {
            return new Issuer(in);
        }

        @Override
        public Issuer[] newArray(int size) {
            return new Issuer[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeString(this.id);
        out.writeString(this.name);
        out.writeString(this.secureThumbnail);
        out.writeString(this.thumbnail);
    }
}