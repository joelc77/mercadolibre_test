package cerviniojoel.mercadolibretest.interactors;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import cerviniojoel.mercadolibretest.R;
import cerviniojoel.mercadolibretest.daos.PaymentMethodDAO;
import cerviniojoel.mercadolibretest.interfaces.ResultListener;
import cerviniojoel.mercadolibretest.models.CardIssuer;
import cerviniojoel.mercadolibretest.models.PaymentMethod;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.observers.DisposableObserver;

import java.util.List;

public class SeleccionarBancoFragmentInteractorImpl implements SeleccionarBancoFragmentInteractor {
    final String LOG_TAG = SeleccionarBancoFragmentInteractorImpl.class.getSimpleName();
    private Context mContext;

    public SeleccionarBancoFragmentInteractorImpl(Context mContext) {
        this.mContext = mContext;
    }


    @Override
    public Observable<List<CardIssuer>> getCardIssuers(String publicKey, String paymentMethodId) {
        PaymentMethodDAO paymentMethodDAO = new PaymentMethodDAO();
        return paymentMethodDAO.getCardIssuersFromServer(publicKey, paymentMethodId);
    }

    @Override
    public Observer getObserverCardIssuers(final ResultListener resultListener) {
        return new DisposableObserver<List<CardIssuer>>() {
            @Override
            public void onNext(List<CardIssuer> cardIssuerList) {
                if(cardIssuerList != null && cardIssuerList.size()>0) {
                    resultListener.onSuccess(cardIssuerList);
                }
                else {
                    resultListener.onError(mContext.getString(R.string.msj_error_generico_metodo_pago));
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.e(LOG_TAG, "getObserverCardIssuers(): " + e.toString());
                resultListener.onError(mContext.getString(R.string.msj_error_generico));
            }

            @Override
            public void onComplete() {

            }
        };
    }


}
