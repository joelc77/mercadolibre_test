package cerviniojoel.mercadolibretest.models;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PaymentMethod implements Parcelable {
    private String id;
    private String name;
    @SerializedName("payment_type_id")
    private String paymentTypeId;
    private String status;
    @SerializedName("secure_thumbnail")
    private String secureThumbnail;
    private String thumbnail;
    @SerializedName("deferred_capture")
    private String deferredCapture;
    private List<Setting> settings;
    @SerializedName("additional_info_needed")
    private List<String> additionalInfoNeeded;
    @SerializedName("min_allowed_amount")
    private String minAllowedAmount;
    @SerializedName("max_allowed_amount")
    private String maxAllowedAmount;
    @SerializedName("accreditation_time")
    private String accreditationTime;
    @SerializedName("processing_modes")
    private List<String> processingModes;


    public PaymentMethod() {
    }

    public PaymentMethod(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.paymentTypeId = in.readString();
        this.status = in.readString();
        this.secureThumbnail = in.readString();
        this.thumbnail = in.readString();
        this.deferredCapture = in.readString();
        in.readTypedList(this.settings, Setting.CREATOR);
        in.readStringList(this.additionalInfoNeeded);
        this.minAllowedAmount = in.readString();
        this.maxAllowedAmount = in.readString();
        this.accreditationTime = in.readString();
        in.readStringList(this.processingModes);
    }



    public PaymentMethod(String id, String name, String paymentTypeId, String status, String secureThumbnail,
                         String thumbnail, String deferredCapture, List<Setting> settings, List<String> additionalInfoNeeded,
                         String minAllowedAmount, String maxAllowedAmount, String accreditationTime, List<String> processingModes) {
        this.id = id;
        this.name = name;
        this.paymentTypeId = paymentTypeId;
        this.status = status;
        this.secureThumbnail = secureThumbnail;
        this.thumbnail = thumbnail;
        this.deferredCapture = deferredCapture;
        this.settings = settings;
        this.additionalInfoNeeded = additionalInfoNeeded;
        this.minAllowedAmount = minAllowedAmount;
        this.maxAllowedAmount = maxAllowedAmount;
        this.accreditationTime = accreditationTime;
        this.processingModes = processingModes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(String paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSecureThumbnail() {
        return secureThumbnail;
    }

    public void setSecureThumbnail(String secureThumbnail) {
        this.secureThumbnail = secureThumbnail;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDeferredCapture() {
        return deferredCapture;
    }

    public void setDeferredCapture(String deferredCapture) {
        this.deferredCapture = deferredCapture;
    }

    public List<Setting> getSettings() {
        return settings;
    }

    public void setSettings(List<Setting> settings) {
        this.settings = settings;
    }

    public List<String> getAdditionalInfoNeeded() {
        return additionalInfoNeeded;
    }

    public void setAdditionalInfoNeeded(List<String> additionalInfoNeeded) {
        this.additionalInfoNeeded = additionalInfoNeeded;
    }

    public String getMinAllowedAmount() {
        return minAllowedAmount;
    }

    public void setMinAllowedAmount(String minAllowedAmount) {
        this.minAllowedAmount = minAllowedAmount;
    }

    public String getMaxAllowedAmount() {
        return maxAllowedAmount;
    }

    public void setMaxAllowedAmount(String maxAllowedAmount) {
        this.maxAllowedAmount = maxAllowedAmount;
    }

    public String getAccreditationTime() {
        return accreditationTime;
    }

    public void setAccreditationTime(String accreditationTime) {
        this.accreditationTime = accreditationTime;
    }

    public List<String> getProcessingModes() {
        return processingModes;
    }

    public void setProcessingModes(List<String> processingModes) {
        this.processingModes = processingModes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PaymentMethod> CREATOR = new Creator<PaymentMethod>() {
        @Override
        public PaymentMethod createFromParcel(Parcel in) {
            return new PaymentMethod(in);
        }

        @Override
        public PaymentMethod[] newArray(int size) {
            return new PaymentMethod[size];
        }
    };

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeString(  this.id);
        out.writeString(this.name);
        out.writeString(this.paymentTypeId);
        out.writeString(  this.status);
        out.writeString(this.secureThumbnail);
        out.writeString(this.thumbnail);
        out.writeString(  this.deferredCapture);
//        this.settings = settings;
//        out.writeTypedList(this.settings);

        out.writeStringList(this.additionalInfoNeeded);
        out.writeString(this.minAllowedAmount);
        out.writeString(this.maxAllowedAmount);
        out.writeString(this.accreditationTime);
        out.writeStringList(this.processingModes);

    }
}


