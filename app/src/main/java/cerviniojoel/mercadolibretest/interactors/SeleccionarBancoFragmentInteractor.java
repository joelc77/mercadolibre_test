package cerviniojoel.mercadolibretest.interactors;

import cerviniojoel.mercadolibretest.interfaces.ResultListener;
import cerviniojoel.mercadolibretest.models.CardIssuer;
import cerviniojoel.mercadolibretest.models.PaymentMethod;
import io.reactivex.Observable;
import io.reactivex.Observer;

import java.util.List;

public interface SeleccionarBancoFragmentInteractor {

    Observable<List<CardIssuer>> getCardIssuers(String publicKey, String paymentMethodId);
    Observer getObserverCardIssuers(ResultListener<List<CardIssuer>> resultListener);

}
