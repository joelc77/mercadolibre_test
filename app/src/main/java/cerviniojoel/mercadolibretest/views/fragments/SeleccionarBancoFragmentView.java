package cerviniojoel.mercadolibretest.views.fragments;

import cerviniojoel.mercadolibretest.models.CardIssuer;
import cerviniojoel.mercadolibretest.models.PaymentMethod;

import java.util.List;

public interface SeleccionarBancoFragmentView {

    void showResultCardIssuers(List<CardIssuer> cardIssuers);
    void showErrorCardIssuers(String error);

    void showProgress();

    void hideProgress();
}
