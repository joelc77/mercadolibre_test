package cerviniojoel.mercadolibretest.interfaces;

import cerviniojoel.mercadolibretest.models.CardIssuer;
import cerviniojoel.mercadolibretest.models.PaymentMethod;

public interface BancoViewHolderListener {
    void onClickBancoViewHolder(CardIssuer cardIssuer);
}
