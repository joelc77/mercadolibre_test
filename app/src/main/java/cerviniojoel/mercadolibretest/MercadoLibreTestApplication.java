package cerviniojoel.mercadolibretest;

import android.app.Application;
import com.facebook.drawee.backends.pipeline.Fresco;

public class MercadoLibreTestApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Fresco.initialize(this);
    }
}
