package cerviniojoel.mercadolibretest.views.holders;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import cerviniojoel.mercadolibretest.R;
import cerviniojoel.mercadolibretest.interfaces.PaymentMethodViewHolderListener;
import cerviniojoel.mercadolibretest.models.PaymentMethod;
import com.facebook.drawee.view.SimpleDraweeView;

public class MetodoPagoViewHolder extends BaseViewHolder {

    @BindView(R.id.tv_titulo)
    TextView tvTitulo;

    @BindView(R.id.sdv_imagen_medio_pago)
    SimpleDraweeView sdvImagenMedioPago;

    PaymentMethodViewHolderListener paymentMethodViewHolderListener;

    public MetodoPagoViewHolder(View itemView, Activity activity, PaymentMethodViewHolderListener paymentMethodViewHolderListener) {
        super(itemView,activity);
        this.paymentMethodViewHolderListener = paymentMethodViewHolderListener;
    }

    @Override
    public void render(Object item, int posicion) {
        if(item instanceof PaymentMethod){
            final PaymentMethod paymentMethod = (PaymentMethod) item;
            this.tvTitulo.setText(paymentMethod.getName());
            if(paymentMethod.getSecureThumbnail() != null && !paymentMethod.getSecureThumbnail().equalsIgnoreCase("")) {
                this.sdvImagenMedioPago.setVisibility(View.VISIBLE);
                this.sdvImagenMedioPago.setImageURI(paymentMethod.getSecureThumbnail());
            }else {
                this.sdvImagenMedioPago.setVisibility(View.GONE);
            }
            this.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MetodoPagoViewHolder.this.paymentMethodViewHolderListener.onClickPaymentMethodViewHolder(paymentMethod);
                }
            });
        }
    }
}
