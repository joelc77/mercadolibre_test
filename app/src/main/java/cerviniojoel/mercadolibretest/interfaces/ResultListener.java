package cerviniojoel.mercadolibretest.interfaces;

public interface ResultListener<T> {

    void onSuccess(T result);
    void onError(String error);
}
