package cerviniojoel.mercadolibretest.interfaces;

import cerviniojoel.mercadolibretest.models.PaymentMethod;

public interface PaymentMethodViewHolderListener {
    void onClickPaymentMethodViewHolder(PaymentMethod paymentMethod);
}
