package cerviniojoel.mercadolibretest.views.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import cerviniojoel.mercadolibretest.R;
import cerviniojoel.mercadolibretest.adapters.BancoAdapter;
import cerviniojoel.mercadolibretest.core.Constantes;
import cerviniojoel.mercadolibretest.core.utils.FragmentHelper;
import cerviniojoel.mercadolibretest.interfaces.BancoViewHolderListener;
import cerviniojoel.mercadolibretest.models.CardIssuer;
import cerviniojoel.mercadolibretest.models.PaymentMethod;
import cerviniojoel.mercadolibretest.presenters.SeleccionarBancoFragmentPresenterImpl;

import java.util.List;

public class SeleccionarBancoFragment extends BaseFragment implements SeleccionarBancoFragmentView, BancoViewHolderListener {
    private static final String LOG_TAG = SeleccionarBancoFragment.class.getSimpleName();

    @BindView(R.id.rv_bancos)
    RecyclerView rvBancos;


    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.container_reload_y_msj)
    View containerReloadYMsj;

    @BindView(R.id.iv_reintentar)
    ImageView ivReintentar;

    @BindView(R.id.tv_msj_error)
    TextView tvMsjError;

    BancoAdapter bancoAdapter = null;
    PaymentMethod metodoPago;
    List<CardIssuer> cardIssuerList;
    String amount;

    SeleccionarBancoFragmentPresenterImpl seleccionarBancoFragmentPresenterImpl;

    public static SeleccionarBancoFragment newInstance(String amount, PaymentMethod paymentMethod) {
        SeleccionarBancoFragment f = new SeleccionarBancoFragment();
        Bundle args = new Bundle();
        args.putString(Constantes.EXTRA_AMOUNT, amount);
        args.putParcelable(Constantes.EXTRA_PAYMENT_METHOD, paymentMethod);
        f.setArguments(args);
        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.init();

    }

    private void init() {
        try {
            this.amount = getArguments().getString(Constantes.EXTRA_AMOUNT);
            this.metodoPago = getArguments().getParcelable(Constantes.EXTRA_PAYMENT_METHOD);
            if (this.metodoPago != null) {
                if (bancoAdapter == null) {
                    this.seleccionarBancoFragmentPresenterImpl = new SeleccionarBancoFragmentPresenterImpl(getActivity(), this);
                    this.seleccionarBancoFragmentPresenterImpl.getCardIssuersFromServer(Constantes.API_KEY, this.metodoPago.getId());
                } else {
                    this.rvBancos.setLayoutManager(new LinearLayoutManager(getActivity()));
                    this.bancoAdapter = new BancoAdapter(getActivity(), cardIssuerList, this);
                    this.rvBancos.setAdapter(this.bancoAdapter);
                }
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "onViewCreated(): " + e.toString());
        }
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_seleccionar_banco;
    }

    @Override
    public void onClickBancoViewHolder(CardIssuer cardIssuer) {
        FragmentHelper.replaceFragment(this.getFragmentManager(), SeleccionarCuotaFragment.newInstance(this.amount, this.metodoPago, cardIssuer), R.id.container_fragment, false);
    }

    @Override
    public void showResultCardIssuers(List<CardIssuer> cardIssuerList) {
        this.cardIssuerList = cardIssuerList;
        this.rvBancos.setLayoutManager(new LinearLayoutManager(getActivity()));
        this.bancoAdapter = new BancoAdapter(getActivity(), cardIssuerList, this);
        this.rvBancos.setAdapter(this.bancoAdapter);
    }

    @Override
    public void showErrorCardIssuers(String error) {
        this.tvMsjError.setText(error);
        this.containerReloadYMsj.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgress() {
        this.progressBar.setVisibility(View.VISIBLE);
        this.containerReloadYMsj.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        this.progressBar.setVisibility(View.GONE);
    }

    @OnClick(R.id.iv_reintentar)
    public void reintentar(){
        this.seleccionarBancoFragmentPresenterImpl.getCardIssuersFromServer(Constantes.API_KEY, this.metodoPago.getId());
    }
}
