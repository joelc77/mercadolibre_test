package cerviniojoel.mercadolibretest.presenters;

public interface SeleccionarBancoFragmentPresenter {
    void getCardIssuersFromServer(String publicKey, String paymentMethodId);
}

