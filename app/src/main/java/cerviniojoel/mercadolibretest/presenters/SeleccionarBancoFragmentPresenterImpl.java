package cerviniojoel.mercadolibretest.presenters;

import android.content.Context;
import cerviniojoel.mercadolibretest.interactors.SeleccionarBancoFragmentInteractorImpl;
import cerviniojoel.mercadolibretest.interfaces.ResultListener;
import cerviniojoel.mercadolibretest.models.CardIssuer;
import cerviniojoel.mercadolibretest.models.PaymentMethod;
import cerviniojoel.mercadolibretest.views.fragments.SeleccionarBancoFragmentView;

import java.util.List;

public class SeleccionarBancoFragmentPresenterImpl implements SeleccionarBancoFragmentPresenter {

    SeleccionarBancoFragmentView seleccionarBancoFragmentView;
    SeleccionarBancoFragmentInteractorImpl seleccionarBancoFragmentInteractor;

    public SeleccionarBancoFragmentPresenterImpl(Context context, SeleccionarBancoFragmentView seleccionarBancoFragmentView) {
        this.seleccionarBancoFragmentView = seleccionarBancoFragmentView;
        this.seleccionarBancoFragmentInteractor = new SeleccionarBancoFragmentInteractorImpl(context);
    }

    @Override
    public void getCardIssuersFromServer(String publicKey, String paymentMethodId){
        this.seleccionarBancoFragmentView.showProgress();

        this.seleccionarBancoFragmentInteractor.getCardIssuers(publicKey, paymentMethodId).subscribeWith(this.seleccionarBancoFragmentInteractor.getObserverCardIssuers(new ResultListener<List<CardIssuer>>() {
            @Override
            public void onSuccess(List<CardIssuer> cardIssuers) {
                seleccionarBancoFragmentView.hideProgress();
                seleccionarBancoFragmentView.showResultCardIssuers(cardIssuers);
            }

            @Override
            public void onError(String error) {
                SeleccionarBancoFragmentPresenterImpl.this.seleccionarBancoFragmentView.hideProgress();
                seleccionarBancoFragmentView.showErrorCardIssuers(error);
            }
        }));
    }


}
