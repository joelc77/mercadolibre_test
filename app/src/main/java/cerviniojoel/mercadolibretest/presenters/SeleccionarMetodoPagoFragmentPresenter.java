package cerviniojoel.mercadolibretest.presenters;

public interface SeleccionarMetodoPagoFragmentPresenter {
    void getPaymentMethodsFromServer(String publicKey, String amount);
}

