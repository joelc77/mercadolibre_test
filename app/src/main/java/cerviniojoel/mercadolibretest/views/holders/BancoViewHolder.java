package cerviniojoel.mercadolibretest.views.holders;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import cerviniojoel.mercadolibretest.R;
import cerviniojoel.mercadolibretest.interfaces.BancoViewHolderListener;
import cerviniojoel.mercadolibretest.interfaces.PaymentMethodViewHolderListener;
import cerviniojoel.mercadolibretest.models.CardIssuer;
import cerviniojoel.mercadolibretest.models.PaymentMethod;
import com.facebook.drawee.view.SimpleDraweeView;

public class BancoViewHolder extends BaseViewHolder {

    @BindView(R.id.tv_titulo)
    TextView tvTitulo;

    @BindView(R.id.sdv_imagen_banco)
    SimpleDraweeView sdvImagenBanco;

    BancoViewHolderListener bancoViewHolderListener;

    public BancoViewHolder(View itemView, Activity activity, BancoViewHolderListener bancoViewHolderListener) {
        super(itemView,activity);
        this.bancoViewHolderListener = bancoViewHolderListener;
    }

    @Override
    public void render(Object item, int posicion) {
        if(item instanceof CardIssuer){
            final CardIssuer cardIssuer = (CardIssuer) item;
            this.tvTitulo.setText(cardIssuer.getName());
            if(!cardIssuer.getSecureThumbnail().equalsIgnoreCase("")) {
                this.sdvImagenBanco.setVisibility(View.VISIBLE);
                this.sdvImagenBanco.setImageURI(cardIssuer.getSecureThumbnail());
            }
            else{
                this.sdvImagenBanco.setVisibility(View.GONE);
            }
            this.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    BancoViewHolder.this.bancoViewHolderListener.onClickBancoViewHolder(cardIssuer);
                }
            });
        }
    }
}
