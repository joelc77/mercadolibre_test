package cerviniojoel.mercadolibretest.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import cerviniojoel.mercadolibretest.R;
import cerviniojoel.mercadolibretest.interfaces.BancoViewHolderListener;
import cerviniojoel.mercadolibretest.models.CardIssuer;
import cerviniojoel.mercadolibretest.views.holders.BancoViewHolder;

import java.util.List;

public class BancoAdapter extends RecyclerView.Adapter<BancoViewHolder> {

    Activity activity;
    List<CardIssuer> cardIssuerList;
    BancoViewHolderListener bancoViewHolderListener;

    public BancoAdapter(Activity activity, List<CardIssuer> cardIssuerList, BancoViewHolderListener bancoViewHolderListener) {
        this.activity = activity;
        this.cardIssuerList = cardIssuerList;
        this.bancoViewHolderListener = bancoViewHolderListener;
    }

    @NonNull
    @Override
    public BancoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(this.activity).inflate(R.layout.holder_banco, viewGroup, false);
        return new BancoViewHolder(itemView, this.activity, this.bancoViewHolderListener);
    }

    @Override
    public void onBindViewHolder(@NonNull BancoViewHolder paymentMethodViewHolder, int i) {
        CardIssuer cardIssuer = this.cardIssuerList.get(i);
        paymentMethodViewHolder.render(cardIssuer, i);
    }

    @Override
    public int getItemCount() {
        return this.cardIssuerList.size();
    }
}
