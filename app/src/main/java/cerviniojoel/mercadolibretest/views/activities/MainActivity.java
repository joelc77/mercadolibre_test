package cerviniojoel.mercadolibretest.views.activities;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import cerviniojoel.mercadolibretest.R;
import cerviniojoel.mercadolibretest.core.utils.FragmentHelper;
import cerviniojoel.mercadolibretest.interfaces.SeleccionarCuotaFragmentListener;
import cerviniojoel.mercadolibretest.models.CardIssuer;
import cerviniojoel.mercadolibretest.models.PayerCost;
import cerviniojoel.mercadolibretest.models.PaymentMethod;
import cerviniojoel.mercadolibretest.views.MsjExitoDialogFragment;
import cerviniojoel.mercadolibretest.views.fragments.IngresarMontoFragment;

public class MainActivity extends BaseActivity implements SeleccionarCuotaFragmentListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.init();
    }

    private void init() {
        this.setDisplayHome();
        FragmentHelper.replaceFragment(this.getSupportFragmentManager(), new IngresarMontoFragment(), R.id.container_fragment, false);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public void onBackPressed() {
        if (FragmentHelper.replaceBackFragment(this.getSupportFragmentManager(),R.id.container_fragment)) {
            super.onBackPressed();
        }else{
            this.finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
               onBackPressed();
               return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCompletePago(String amount, PaymentMethod paymentMethod, CardIssuer cardIssuer, PayerCost payerCost) {
        FragmentHelper.removeFragments(this.getSupportFragmentManager());
        FragmentHelper.replaceFragment(this.getSupportFragmentManager(), new IngresarMontoFragment(), R.id.container_fragment,false);
        FragmentHelper.showDialogFragment(this.getSupportFragmentManager(), MsjExitoDialogFragment.newInstance(amount, paymentMethod, cardIssuer, payerCost));
    }
}
