package cerviniojoel.mercadolibretest.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import cerviniojoel.mercadolibretest.R;
import cerviniojoel.mercadolibretest.interfaces.PaymentMethodViewHolderListener;
import cerviniojoel.mercadolibretest.models.PaymentMethod;
import cerviniojoel.mercadolibretest.views.holders.MetodoPagoViewHolder;

import java.util.List;

public class PaymentsMethodAdapter extends RecyclerView.Adapter<MetodoPagoViewHolder> {

    Activity activity;
    List<PaymentMethod> paymentMethods;
    PaymentMethodViewHolderListener paymentMethodViewHolderListener;

    public PaymentsMethodAdapter(Activity activity, List<PaymentMethod> paymentMethods, PaymentMethodViewHolderListener paymentMethodViewHolderListener) {
        this.activity = activity;
        this.paymentMethods = paymentMethods;
        this.paymentMethodViewHolderListener = paymentMethodViewHolderListener;
    }

    @NonNull
    @Override
    public MetodoPagoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(this.activity).inflate(R.layout.holder_metodo_pago, viewGroup, false);
        return new MetodoPagoViewHolder(itemView, this.activity, this.paymentMethodViewHolderListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MetodoPagoViewHolder paymentMethodViewHolder, int i) {
        PaymentMethod paymentMethod = this.paymentMethods.get(i);
        paymentMethodViewHolder.render(paymentMethod, i);
    }

    @Override
    public int getItemCount() {
        return this.paymentMethods.size();
    }

    public void reloadList(List<PaymentMethod> paymentMethods){
        this.paymentMethods.clear();
        this.paymentMethods = paymentMethods;
        this.notifyDataSetChanged();

    }
}
