package cerviniojoel.mercadolibretest.core.utils;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class KeyBoardHelper {
    private static final String LOG_TAG = FragmentHelper.class.getSimpleName();

    public static void hideKeyBoard(Activity activity) {
        try {
            if (activity != null) {
                View view = activity.getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "Exception en hideKeyBoard: " + e.getMessage());
        }
    }
}
