package cerviniojoel.mercadolibretest.core;

public  class Constantes  {
    public static final String API_KEY = "444a9ef5-8a6b-429f-abdf-587639155d88";
    public static final String EXTRA_AMOUNT = "EXTRA_AMOUNT";
    public static final String EXTRA_PAYMENT_METHOD = "EXTRA_PAYMENT_METHOD";
    public static final String EXTRA_CARD_ISSUER = "EXTRA_CARD_ISSUER";
    public static final String EXTRA_PAYER_COST = "EXTRA_PAYER_COST";
}
