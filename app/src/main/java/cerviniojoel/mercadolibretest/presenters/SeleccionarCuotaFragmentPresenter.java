package cerviniojoel.mercadolibretest.presenters;

import cerviniojoel.mercadolibretest.models.Installments;

import java.util.List;

public interface SeleccionarCuotaFragmentPresenter {
    void getInstallmentsFromServer(String publicKey, String paymentMethodId, String amount, String issuerId);
    void getListaCuotas(List<Installments> installmentsList);
    void getPayerCost(List<Installments> installmentsList, String cuota);
}

