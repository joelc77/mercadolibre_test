package cerviniojoel.mercadolibretest.views.fragments;

import cerviniojoel.mercadolibretest.models.CardIssuer;
import cerviniojoel.mercadolibretest.models.Installments;
import cerviniojoel.mercadolibretest.models.PayerCost;

import java.util.List;

public interface SeleccionarCuotaFragmentView {

    void showResultInstallments(List<Installments> installmentsList);
    void showErrorInstallments(String error);

    void showResultCuotas(List<String> cuotasList);
    void showErrorCuotas(String error);

    void showResultPayerCost(PayerCost payerCost);
    void showErrorPayerCost(String error);

    void showProgress();

    void hideProgress();
}
