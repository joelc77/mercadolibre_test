package cerviniojoel.mercadolibretest.models;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

public class Bin implements Parcelable {
    String pattern;
    @SerializedName("installments_pattern")
    String installmentsPattern;
    @SerializedName("exclusion_pattern")
    String exclusionPattern;

    public Bin() {
    }


    public Bin(Parcel in) {
        this.pattern = in.readString();
        this.installmentsPattern = in.readString();
        this.exclusionPattern = in.readString();
    }

    public Bin(String pattern, String installmentsPattern, String exclusionPattern) {
        this.pattern = pattern;
        this.installmentsPattern = installmentsPattern;
        this.exclusionPattern = exclusionPattern;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public String getInstallmentsPattern() {
        return installmentsPattern;
    }

    public void setInstallmentsPattern(String installmentsPattern) {
        this.installmentsPattern = installmentsPattern;
    }

    public String getExclusionPattern() {
        return exclusionPattern;
    }

    public void setExclusionPattern(String exclusionPattern) {
        this.exclusionPattern = exclusionPattern;
    }

    public static final Creator<Bin> CREATOR = new Creator<Bin>() {
        @Override
        public Bin createFromParcel(Parcel in) {
            return new Bin(in);
        }

        @Override
        public Bin[] newArray(int size) {
            return new Bin[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeString(this.pattern);
        out.writeString(this.installmentsPattern);
        out.writeString(this.exclusionPattern);
    }
}