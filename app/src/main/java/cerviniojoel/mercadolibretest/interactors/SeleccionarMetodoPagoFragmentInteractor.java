package cerviniojoel.mercadolibretest.interactors;

import cerviniojoel.mercadolibretest.interfaces.ResultListener;
import cerviniojoel.mercadolibretest.models.CardIssuer;
import cerviniojoel.mercadolibretest.models.PaymentMethod;
import io.reactivex.Observable;
import io.reactivex.Observer;

import java.util.ArrayList;
import java.util.List;

public interface SeleccionarMetodoPagoFragmentInteractor {

    Observable<List<PaymentMethod>> getPaymentMethods(String publicKey);
    Observer getObserverPaymentMethods(String amount, ResultListener<List<PaymentMethod>> resultListener);


}
