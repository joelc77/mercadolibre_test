package cerviniojoel.mercadolibretest.presenters;

import android.content.Context;
import cerviniojoel.mercadolibretest.interactors.SeleccionarCuotaFragmentInteractorImpl;
import cerviniojoel.mercadolibretest.interfaces.ResultListener;
import cerviniojoel.mercadolibretest.models.Installments;
import cerviniojoel.mercadolibretest.models.PayerCost;
import cerviniojoel.mercadolibretest.views.fragments.SeleccionarCuotaFragmentView;

import java.util.List;

public class SeleccionarCuotaFragmentPresenterImpl implements SeleccionarCuotaFragmentPresenter {

    SeleccionarCuotaFragmentView seleccionarCuotaFragmentView;
    SeleccionarCuotaFragmentInteractorImpl seleccionarCuotaFragmentInteractor;

    public SeleccionarCuotaFragmentPresenterImpl(Context context, SeleccionarCuotaFragmentView seleccionarCuotaFragmentView) {
        this.seleccionarCuotaFragmentView = seleccionarCuotaFragmentView;
        this.seleccionarCuotaFragmentInteractor = new SeleccionarCuotaFragmentInteractorImpl(context);
    }

    @Override
    public void getInstallmentsFromServer(String publicKey, String paymentMethodId, String amount, String issuerId) {
        this.seleccionarCuotaFragmentView.showProgress();
        this.seleccionarCuotaFragmentInteractor.getInstallments(publicKey, paymentMethodId, amount, issuerId).subscribeWith(this.seleccionarCuotaFragmentInteractor.getObserverInstallments(new ResultListener<List<Installments>>() {
            @Override
            public void onSuccess(List<Installments> installmentsList) {
                seleccionarCuotaFragmentView.hideProgress();
                seleccionarCuotaFragmentView.showResultInstallments(installmentsList);
            }

            @Override
            public void onError(String error) {
                seleccionarCuotaFragmentView.hideProgress();
                seleccionarCuotaFragmentView.showErrorInstallments(error);
            }
        }));
    }

    @Override
    public void getListaCuotas(List<Installments> installmentsList) {
        this.seleccionarCuotaFragmentInteractor.getListaCuotas(installmentsList, new ResultListener<List<String>>() {
            @Override
            public void onSuccess(List<String> result) {
                seleccionarCuotaFragmentView.showResultCuotas(result);
            }

            @Override
            public void onError(String error) {
                seleccionarCuotaFragmentView.showErrorCuotas(error);
            }
        });
    }

    @Override
    public void getPayerCost(List<Installments> installmentsList, String cuota) {
        this.seleccionarCuotaFragmentInteractor.getPayerCost(installmentsList, cuota, new ResultListener<PayerCost>() {
            @Override
            public void onSuccess(PayerCost result) {
                seleccionarCuotaFragmentView.showResultPayerCost(result);
            }

            @Override
            public void onError(String error) {
                seleccionarCuotaFragmentView.showErrorPayerCost(error);
            }
        });
    }
}
