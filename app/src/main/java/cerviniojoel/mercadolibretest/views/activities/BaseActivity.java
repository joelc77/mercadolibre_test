package cerviniojoel.mercadolibretest.views.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import cerviniojoel.mercadolibretest.R;

public abstract class BaseActivity extends AppCompatActivity {
    private final String LOG_TAG = BaseActivity.class.getSimpleName();

    @Nullable
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        bindViews();

        if(this.toolbar != null) {
            this.setSupportActionBar(this.toolbar);
        }
    }

    protected abstract int getLayoutId();

    private void bindViews(){
        ButterKnife.bind(this);
    }

    protected void setDisplayHome(){
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            this.toolbar.setTitleTextColor(ContextCompat.getColor(this, android.R.color.white));
        }
    }


}
