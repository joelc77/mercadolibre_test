package cerviniojoel.mercadolibretest.interactors;

import cerviniojoel.mercadolibretest.interfaces.ResultListener;
import cerviniojoel.mercadolibretest.models.CardIssuer;
import cerviniojoel.mercadolibretest.models.Installments;
import cerviniojoel.mercadolibretest.models.PayerCost;
import io.reactivex.Observable;
import io.reactivex.Observer;

import java.util.List;

public interface SeleccionarCuotaFragmentInteractor {

    Observable<List<Installments>> getInstallments(String publicKey, String paymentMethodId, String amount, String issuerId);
    Observer getObserverInstallments(ResultListener<List<Installments>> resultListener);

    void getListaCuotas(List<Installments> installmentsList, ResultListener<List<String>> resultListener);

    void getPayerCost(List<Installments> installmentsList, String cuota, ResultListener<PayerCost> resultListener);

}
