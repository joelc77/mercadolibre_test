package cerviniojoel.mercadolibretest.views.fragments;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import butterknife.BindView;
import butterknife.OnClick;
import cerviniojoel.mercadolibretest.R;
import cerviniojoel.mercadolibretest.core.utils.FragmentHelper;
import cerviniojoel.mercadolibretest.core.utils.KeyBoardHelper;

public class IngresarMontoFragment extends BaseFragment {
    private static final String LOG_TAG = IngresarMontoFragment.class.getSimpleName();

    @BindView(R.id.btn_siguiente)
    Button btnSiguiente;

    @BindView(R.id.actv_ingresar_monto)
    AutoCompleteTextView actvIngresarMonto;

    @BindView(R.id.til_ingresar_monto)
    TextInputLayout tilIngresarMonto;


    @OnClick(R.id.btn_siguiente)
    public void callMetodoPagoFragment() {
        if (!this.actvIngresarMonto.getText().toString().equalsIgnoreCase("")) {
            float amount = Float.parseFloat(actvIngresarMonto.getText().toString());
            if (amount > 0) {
                KeyBoardHelper.hideKeyBoard(this.getActivity());
                FragmentHelper.replaceFragment(this.getFragmentManager(), SeleccionarMetodoPagoFragment.newInstance(actvIngresarMonto.getText().toString()), R.id.container_fragment, false);
            }
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.init();
    }

    private void init() {
        this.actvIngresarMonto.addTextChangedListener(onChangeIngresarMontoText());
        this.actvIngresarMonto.setOnKeyListener(getActvIngresarMontoOnKeyListener());
    }


    protected int getLayoutId() {
        return R.layout.fragment_ingresar_monto;
    }

    private TextWatcher onChangeIngresarMontoText() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                setNormalStyleInputTextLayout(tilIngresarMonto);
                btnSiguiente.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (actvIngresarMonto.getText().toString().length() > 0 && Float.parseFloat(actvIngresarMonto.getText().toString()) > 0) {
                    setNormalStyleInputTextLayout(tilIngresarMonto);
                    btnSiguiente.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                } else {
                    setErrorStyleInputTextLayout(tilIngresarMonto);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
    }

    protected void setNormalStyleInputTextLayout(TextInputLayout textInputLayout) {
        if (textInputLayout != null) {
            textInputLayout.getEditText().getBackground().setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
            textInputLayout.setHintTextAppearance(R.style.login_text_label);
        }
    }

    public void setErrorStyleInputTextLayout(TextInputLayout textInputLayout) {
        if (textInputLayout != null) {
            textInputLayout.getEditText().getBackground().setColorFilter(ContextCompat.getColor(getContext(), android.R.color.holo_red_dark), PorterDuff.Mode.SRC_ATOP);
            textInputLayout.setHintTextAppearance(R.style.login_text_label_error);
        }
    }

    private View.OnKeyListener getActvIngresarMontoOnKeyListener() {
        return new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            callMetodoPagoFragment();
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        };
    }
}
