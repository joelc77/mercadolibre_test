package cerviniojoel.mercadolibretest.views.holders;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.ButterKnife;

public abstract class BaseViewHolder extends RecyclerView.ViewHolder {
    Activity activity;

    public BaseViewHolder(View itemView, Activity activity) {
        super(itemView);
        this.activity = activity;
        ButterKnife.bind(this, itemView);
    }


    public abstract void render(Object item, int posicion);

}
