package cerviniojoel.mercadolibretest.interactors;

import android.content.Context;
import android.util.Log;
import cerviniojoel.mercadolibretest.R;
import cerviniojoel.mercadolibretest.daos.PaymentMethodDAO;
import cerviniojoel.mercadolibretest.interfaces.ResultListener;
import cerviniojoel.mercadolibretest.models.PaymentMethod;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.observers.DisposableObserver;

import java.util.ArrayList;
import java.util.List;

public class SeleccionarMetodoPagoFragmentInteractorImpl implements SeleccionarMetodoPagoFragmentInteractor {
    final String LOG_TAG = SeleccionarMetodoPagoFragmentInteractorImpl.class.getSimpleName();
    private Context mContext;

    public SeleccionarMetodoPagoFragmentInteractorImpl(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public Observable<List<PaymentMethod>> getPaymentMethods(String publicKey) {
        PaymentMethodDAO paymentMethodDAO = new PaymentMethodDAO();
        return paymentMethodDAO.getPaymentMethodsFromServer(publicKey);

    }

    @Override
    public Observer getObserverPaymentMethods(final String amount, final ResultListener resultListener) {
        return new DisposableObserver<List<PaymentMethod>>() {
            @Override
            public void onNext(List<PaymentMethod> paymentMethods) {
                try {
                    List<PaymentMethod> paymentMethodList = filtrarMetodosPagoPorValorIngresado(paymentMethods, amount);

                    if (paymentMethodList.size() > 0) {
                        resultListener.onSuccess(paymentMethodList);
                    } else {
                        resultListener.onError("No hay ningun medio de pago que acepte el valor ingresado, por favor prueba con otro monto");
                    }
                } catch (Exception e) {
                    resultListener.onError("ha ocurrido un error inesperado");
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.e(LOG_TAG, "getObserverPaymentMethods(): " + e.toString());
                resultListener.onError(mContext.getString(R.string.msj_error_generico));
            }

            @Override
            public void onComplete() {

            }
        };
    }

    private List<PaymentMethod> filtrarMetodosPagoPorValorIngresado(List<PaymentMethod> paymentMethods, String amount) {
        List<PaymentMethod> paymentMethodList = new ArrayList<>();
        for (int i = 0; i < paymentMethods.size(); i++) {
            PaymentMethod paymentMethod = paymentMethods.get(i);

            int retval1 = Float.compare(Float.valueOf(paymentMethod.getMinAllowedAmount()), Float.valueOf(amount));
            int retval2 = Float.compare(Float.valueOf(paymentMethod.getMaxAllowedAmount()), Float.valueOf(amount));

            if (retval1 <= 0 && retval2 >= 0) {
                paymentMethodList.add(paymentMethod);
            }
        }
        return paymentMethodList;

    }


}
