package cerviniojoel.mercadolibretest.models;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

public class CardNumber implements Parcelable {
    String validation;
    String length;

    public CardNumber() {
    }

    public CardNumber(Parcel in) {
        this.validation = in.readString();
        this.length = in.readString();
    }

    public CardNumber(String validation, String length) {
        this.validation = validation;
        this.length = length;
    }

    public String getValidation() {
        return validation;
    }

    public void setValidation(String validation) {
        this.validation = validation;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public static final Creator<CardNumber> CREATOR = new Creator<CardNumber>() {
        @Override
        public CardNumber createFromParcel(Parcel in) {
            return new CardNumber(in);
        }

        @Override
        public CardNumber[] newArray(int size) {
            return new CardNumber[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeString(  this.validation);
        out.writeString(this.length);

    }
}