package cerviniojoel.mercadolibretest.views;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import cerviniojoel.mercadolibretest.R;
import cerviniojoel.mercadolibretest.core.Constantes;
import cerviniojoel.mercadolibretest.interfaces.SeleccionarCuotaFragmentListener;
import cerviniojoel.mercadolibretest.models.CardIssuer;
import cerviniojoel.mercadolibretest.models.PayerCost;
import cerviniojoel.mercadolibretest.models.PaymentMethod;
import cerviniojoel.mercadolibretest.views.fragments.SeleccionarCuotaFragment;

public class MsjExitoDialogFragment extends DialogFragment {

    @BindView(R.id.tv_msj_exito_medio_pago)
    TextView tvMsjExitoMedioPago;

    @BindView(R.id.tv_msj_exito_banco)
    TextView tvMsjExitoBanco;

    @BindView(R.id.tv_msj_exito_cuota)
    TextView tvMsjExitoCuota;


    PaymentMethod metodoPago;
    CardIssuer cardIssuer;
    PayerCost payerCost;
    String amount;

    public static MsjExitoDialogFragment newInstance(String amount, PaymentMethod paymentMethod, CardIssuer cardIssuer, PayerCost payerCost) {
        MsjExitoDialogFragment f = new MsjExitoDialogFragment();
        Bundle args = new Bundle();
        args.putString(Constantes.EXTRA_AMOUNT, amount);
        args.putParcelable(Constantes.EXTRA_PAYMENT_METHOD, paymentMethod);
        args.putParcelable(Constantes.EXTRA_CARD_ISSUER, cardIssuer);
        args.putParcelable(Constantes.EXTRA_PAYER_COST, payerCost);
        f.setArguments(args);
        return f;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_msj_exito, container, false);
        ButterKnife.bind(this, view);
        getDialog().requestWindowFeature(STYLE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.amount = getArguments().getString(Constantes.EXTRA_AMOUNT);
        this.metodoPago = getArguments().getParcelable(Constantes.EXTRA_PAYMENT_METHOD);
        this.cardIssuer = getArguments().getParcelable(Constantes.EXTRA_CARD_ISSUER);
        this.payerCost = getArguments().getParcelable(Constantes.EXTRA_PAYER_COST);

        this.setMessage();
    }

    public void setMessage(){
        this.tvMsjExitoMedioPago.setText("MEDIO DE PAGO: " + this.metodoPago.getName());
        this.tvMsjExitoBanco.setText("BANCO: " + this.cardIssuer.getName());
        this.tvMsjExitoCuota.setText(this.payerCost.getRecommended_message());
    }
}
