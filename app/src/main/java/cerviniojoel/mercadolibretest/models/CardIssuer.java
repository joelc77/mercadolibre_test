package cerviniojoel.mercadolibretest.models;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

public class CardIssuer implements Parcelable {

    String id;
    String name;
    @SerializedName("secure_thumbnail")
    String secureThumbnail;
    String thumbnail;
    @SerializedName("processing_mode")
    String processingMode;

    public CardIssuer(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.secureThumbnail = in.readString();
        this.thumbnail = in.readString();
        this.processingMode = in.readString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecureThumbnail() {
        return secureThumbnail;
    }

    public void setSecureThumbnail(String secureThumbnail) {
        this.secureThumbnail = secureThumbnail;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getProcessingMode() {
        return processingMode;
    }

    public void setProcessingMode(String processingMode) {
        this.processingMode = processingMode;
    }

    public static final Creator<CardIssuer> CREATOR = new Creator<CardIssuer>() {
        @Override
        public CardIssuer createFromParcel(Parcel in) {
            return new CardIssuer(in);
        }

        @Override
        public CardIssuer[] newArray(int size) {
            return new CardIssuer[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeString(this.id);
        out.writeString(this.name);
        out.writeString(this.secureThumbnail);
        out.writeString(this.thumbnail);
        out.writeString(this.processingMode);
    }
}
