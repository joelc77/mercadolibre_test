package cerviniojoel.mercadolibretest.models;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Installments implements Parcelable {

    @SerializedName("payment_method_id")
    private String paymenMethodId;
    @SerializedName("payment_type_id")
    private String paymentTypeId;
    private Issuer issuer;
    @SerializedName("processing_mode")
    private String processingMode;
    @SerializedName("merchant_account_id")
    private String merchantAccountId;
    @SerializedName("payer_costs")
    private List<PayerCost> payerCosts;

    public Installments() {
    }

    public Installments(Parcel in) {
        this.paymenMethodId = in.readString();
        this.paymentTypeId = in.readString();
        in.readParcelable(Issuer.class.getClassLoader());
        this.processingMode = in.readString();
        this.merchantAccountId = in.readString();
        in.readTypedList(this.payerCosts, PayerCost.CREATOR);
    }

    public Installments(String paymenMethodId, String paymentTypeId, Issuer issuer, String processingMode, String merchantAccountId, List<PayerCost> payerCosts) {
        this.paymenMethodId = paymenMethodId;
        this.paymentTypeId = paymentTypeId;
        this.issuer = issuer;
        this.processingMode = processingMode;
        this.merchantAccountId = merchantAccountId;
        this.payerCosts = payerCosts;
    }

    public String getPaymenMethodId() {
        return paymenMethodId;
    }

    public void setPaymenMethodId(String paymenMethodId) {
        this.paymenMethodId = paymenMethodId;
    }

    public String getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(String paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public Issuer getIssuer() {
        return issuer;
    }

    public void setIssuer(Issuer issuer) {
        this.issuer = issuer;
    }

    public String getProcessingMode() {
        return processingMode;
    }

    public void setProcessingMode(String processingMode) {
        this.processingMode = processingMode;
    }

    public String getMerchantAccountId() {
        return merchantAccountId;
    }

    public void setMerchantAccountId(String merchantAccountId) {
        this.merchantAccountId = merchantAccountId;
    }

    public List<PayerCost> getPayerCosts() {
        return payerCosts;
    }

    public void setPayerCosts(List<PayerCost> payerCosts) {
        this.payerCosts = payerCosts;
    }

    public static final Creator<Installments> CREATOR = new Creator<Installments>() {
        @Override
        public Installments createFromParcel(Parcel in) {
            return new Installments(in);
        }

        @Override
        public Installments[] newArray(int size) {
            return new Installments[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeString(this.paymenMethodId);
        out.writeString(this.paymentTypeId);
        out.writeParcelable(issuer, 0);
        out.writeString(this.processingMode);
        out.writeString(this.merchantAccountId);
        out.writeString(this.processingMode);
        out.writeTypedList(this.payerCosts);
    }
}
