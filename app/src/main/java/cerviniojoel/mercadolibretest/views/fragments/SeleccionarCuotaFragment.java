package cerviniojoel.mercadolibretest.views.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import butterknife.BindView;
import butterknife.OnClick;
import cerviniojoel.mercadolibretest.R;
import cerviniojoel.mercadolibretest.core.Constantes;
import cerviniojoel.mercadolibretest.interfaces.SeleccionarCuotaFragmentListener;
import cerviniojoel.mercadolibretest.models.CardIssuer;
import cerviniojoel.mercadolibretest.models.Installments;
import cerviniojoel.mercadolibretest.models.PayerCost;
import cerviniojoel.mercadolibretest.models.PaymentMethod;
import cerviniojoel.mercadolibretest.presenters.SeleccionarCuotaFragmentPresenterImpl;

import java.util.List;

public class SeleccionarCuotaFragment extends BaseFragment implements SeleccionarCuotaFragmentView {
    private static final String LOG_TAG = SeleccionarCuotaFragment.class.getSimpleName();

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.spinner_cuotas)
    Spinner spinnerCuotas;

    @BindView(R.id.tv_detalle)
    TextView tvDetalle;

    @BindView(R.id.container_reload_y_msj)
    View containerReloadYMsj;

    @BindView(R.id.tv_msj_error)
    TextView tvMsjError;

    @BindView(R.id.container_cuotas_y_detalles)
    View containerCuotasYDetalles;

    @BindView(R.id.btn_siguiente)
    Button btnSiguiente;

    SeleccionarCuotaFragmentListener seleccionarCuotaFragmentListener;
    List<Installments> installmentsList;
    PaymentMethod metodoPago;
    CardIssuer cardIssuer;
    PayerCost payerCost;
    String amount;

    SeleccionarCuotaFragmentPresenterImpl seleccionarCuotaFragmentPresenterImpl;

    public static SeleccionarCuotaFragment newInstance(String amount, PaymentMethod paymentMethod, CardIssuer cardIssuer) {
        SeleccionarCuotaFragment f = new SeleccionarCuotaFragment();
        Bundle args = new Bundle();
        args.putString(Constantes.EXTRA_AMOUNT, amount);
        args.putParcelable(Constantes.EXTRA_PAYMENT_METHOD, paymentMethod);
        args.putParcelable(Constantes.EXTRA_CARD_ISSUER, cardIssuer);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof SeleccionarCuotaFragmentListener){
            this.seleccionarCuotaFragmentListener = (SeleccionarCuotaFragmentListener)context;
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.init();
    }

    private void init() {
        try {
            this.amount = getArguments().getString(Constantes.EXTRA_AMOUNT);
            this.metodoPago = getArguments().getParcelable(Constantes.EXTRA_PAYMENT_METHOD);
            this.cardIssuer = getArguments().getParcelable(Constantes.EXTRA_CARD_ISSUER);
            if (this.metodoPago != null) {
                this.seleccionarCuotaFragmentPresenterImpl = new SeleccionarCuotaFragmentPresenterImpl(getActivity(), this);
                this.seleccionarCuotaFragmentPresenterImpl.getInstallmentsFromServer(Constantes.API_KEY, this.metodoPago.getId(), this.amount, cardIssuer.getId());
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "onViewCreated(): " + e.toString());
        }
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_seleccionar_cuota;
    }

    @Override
    public void showResultInstallments(List<Installments> installmentsList) {
        this.installmentsList = installmentsList;
        this.seleccionarCuotaFragmentPresenterImpl.getListaCuotas(installmentsList);
        this.containerCuotasYDetalles.setVisibility(View.VISIBLE);
        this.btnSiguiente.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
    }

    @Override
    public void showErrorInstallments(String error) {
        this.tvMsjError.setText(error);
        this.containerReloadYMsj.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgress() {
        this.progressBar.setVisibility(View.VISIBLE);
        this.containerReloadYMsj.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        this.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showResultCuotas(List<String> cuotasList) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this.getActivity(), android.R.layout.simple_spinner_dropdown_item, cuotasList);
        this.spinnerCuotas.setAdapter(adapter);
        this.spinnerCuotas.setOnItemSelectedListener(onSpinnerCuotasItemSelectedListener());
    }

    @Override
    public void showErrorCuotas(String error) {

    }

    private AdapterView.OnItemSelectedListener onSpinnerCuotasItemSelectedListener(){
        return  new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                seleccionarCuotaFragmentPresenterImpl.getPayerCost(installmentsList, parentView.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        };
    }

    @Override
    public void showResultPayerCost(PayerCost payerCost) {
        if(!payerCost.getRecommended_message().equalsIgnoreCase("")) {
            this.payerCost = payerCost;
            this.tvDetalle.setText(payerCost.getRecommended_message());
        }
    }

    @Override
    public void showErrorPayerCost(String error) {

    }

    @OnClick(R.id.btn_siguiente)
    public void onClickBtnSiguiente(){
        if(installmentsList.size()>0 && this.seleccionarCuotaFragmentListener != null) {
            this.seleccionarCuotaFragmentListener.onCompletePago(this.amount, this.metodoPago, this.cardIssuer, this.payerCost);
        }
    }


}
