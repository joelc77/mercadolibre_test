package cerviniojoel.mercadolibretest.views.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import cerviniojoel.mercadolibretest.R;
import cerviniojoel.mercadolibretest.adapters.PaymentsMethodAdapter;
import cerviniojoel.mercadolibretest.core.Constantes;
import cerviniojoel.mercadolibretest.core.utils.FragmentHelper;
import cerviniojoel.mercadolibretest.interfaces.PaymentMethodViewHolderListener;
import cerviniojoel.mercadolibretest.models.PaymentMethod;
import cerviniojoel.mercadolibretest.presenters.SeleccionarMetodoPagoFragmentPresenterImpl;

import java.util.List;

public class SeleccionarMetodoPagoFragment extends BaseFragment implements SeleccionarMetodoPagoFragmentView, PaymentMethodViewHolderListener {
    private static final String LOG_TAG = SeleccionarMetodoPagoFragment.class.getSimpleName();

    @BindView(R.id.rv_metodos_pago)
    RecyclerView rvMetodoPago;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.container_reload_y_msj)
    View containerReloadYMsj;

    @BindView(R.id.iv_reintentar)
    ImageView ivReintentar;

    @BindView(R.id.tv_msj_error)
    TextView tvMsjError;

    SeleccionarMetodoPagoFragmentPresenterImpl seleccionarMetodoPagoFragmentPresenterImpl;
    List<PaymentMethod> paymentMethodList;
    String amount;
    PaymentMethod paymentMethod;
    PaymentsMethodAdapter paymentsMethodAdapter = null;

    public static SeleccionarMetodoPagoFragment newInstance(String amount) {
        SeleccionarMetodoPagoFragment f = new SeleccionarMetodoPagoFragment();
        Bundle args = new Bundle();
        args.putString(Constantes.EXTRA_AMOUNT, amount);
        f.setArguments(args);
        return f;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (this.paymentsMethodAdapter == null) {
            this.amount = getArguments().getString(Constantes.EXTRA_AMOUNT);
            this.seleccionarMetodoPagoFragmentPresenterImpl = new SeleccionarMetodoPagoFragmentPresenterImpl(getActivity(), this);
            this.seleccionarMetodoPagoFragmentPresenterImpl.getPaymentMethodsFromServer(Constantes.API_KEY, amount);
        } else {
            this.rvMetodoPago.setLayoutManager(new LinearLayoutManager(getActivity()));
            this.paymentsMethodAdapter = new PaymentsMethodAdapter(getActivity(), paymentMethodList, this);
            this.rvMetodoPago.setAdapter(this.paymentsMethodAdapter);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_seleccionar_metodo_pago;
    }

    @Override
    public void onClickPaymentMethodViewHolder(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
        FragmentHelper.replaceFragment(this.getFragmentManager(), SeleccionarBancoFragment.newInstance(this.amount, this.paymentMethod), R.id.container_fragment, false);
    }

    @Override
    public void showResultPaymentMethods(List<PaymentMethod> paymentMethodList) {
        this.paymentMethodList = paymentMethodList;
        this.rvMetodoPago.setLayoutManager(new LinearLayoutManager(getActivity()));
        this.paymentsMethodAdapter = new PaymentsMethodAdapter(getActivity(), paymentMethodList, this);
        this.rvMetodoPago.setAdapter(this.paymentsMethodAdapter);
    }

    @Override
    public void showErrorPaymentMethods(String error) {
        tvMsjError.setText(error);
        containerReloadYMsj.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
        containerReloadYMsj.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }


    @OnClick(R.id.iv_reintentar)
    public void reintentar() {
        this.seleccionarMetodoPagoFragmentPresenterImpl.getPaymentMethodsFromServer(Constantes.API_KEY, this.amount);
    }


}
