package cerviniojoel.mercadolibretest.daos;

import android.content.Context;
import cerviniojoel.mercadolibretest.daos.apis.PaymentMethodClient;
import cerviniojoel.mercadolibretest.models.CardIssuer;
import cerviniojoel.mercadolibretest.models.Installments;
import cerviniojoel.mercadolibretest.models.PaymentMethod;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.GET;
import retrofit2.http.Query;

import java.util.List;

public class PaymentMethodDAO<T>  {
    protected static final String LOG_TAG = PaymentMethodDAO.class.getSimpleName();

    public PaymentMethodDAO() {
    }


    public Observable<List<PaymentMethod>> getPaymentMethodsFromServer(String publicKey){
        return PaymentMethodClient.getInstance().create(PaymentMethodRetrofit.class).getPaymentMethods(publicKey)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    public Observable<List<CardIssuer>> getCardIssuersFromServer(String publicKey, String paymentMethodId){
        return PaymentMethodClient.getInstance().create(PaymentMethodRetrofit.class).getCardIssuers(publicKey, paymentMethodId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    public Observable<List<Installments>> getInstallmentsFromServer(String publicKey, String paymentMethodId, String amount, String issuerId){
        return PaymentMethodClient.getInstance().create(PaymentMethodRetrofit.class).getInstallments(publicKey, paymentMethodId, amount, issuerId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    interface PaymentMethodRetrofit {
        @GET(".")
        Observable<List<PaymentMethod>> getPaymentMethods(@Query("public_key") String publicKey);

        @GET("./card_issuers")
        Observable<List<CardIssuer>> getCardIssuers(@Query("public_key") String publicKey, @Query("payment_method_id") String paymentMethodId);

        @GET("./installments")
        Observable<List<Installments>> getInstallments(@Query("public_key") String publicKey, @Query("payment_method_id") String paymentMethodId,
                                                       @Query("amount")  String amount, @Query("issuer.id" )String issuerId);
    }
}
