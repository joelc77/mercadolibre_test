package cerviniojoel.mercadolibretest.daos.apis;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class PaymentMethodClient {
    private static Retrofit instance = null;
    private static final String URL_API_BASE = "https://api.mercadopago.com/v1/payment_methods/";

    private PaymentMethodClient() {
    }

    public static Retrofit getInstance() {
        if (instance == null) {
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            instance = new Retrofit.Builder()
                    .baseUrl(URL_API_BASE)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(builder.build())
                    .build();
        }
        return instance;
    }
}
