package cerviniojoel.mercadolibretest.presenters;

import android.content.Context;
import cerviniojoel.mercadolibretest.interactors.SeleccionarMetodoPagoFragmentInteractorImpl;
import cerviniojoel.mercadolibretest.interfaces.ResultListener;
import cerviniojoel.mercadolibretest.models.PaymentMethod;
import cerviniojoel.mercadolibretest.views.fragments.SeleccionarMetodoPagoFragmentView;

import java.util.List;

public class SeleccionarMetodoPagoFragmentPresenterImpl implements SeleccionarMetodoPagoFragmentPresenter {

    SeleccionarMetodoPagoFragmentView seleccionarMetodoPagoFragmentView;
    SeleccionarMetodoPagoFragmentInteractorImpl seleccionarMetodoPagoFragmentInteractorImpl;

    public SeleccionarMetodoPagoFragmentPresenterImpl(Context context, SeleccionarMetodoPagoFragmentView seleccionarMetodoPagoFragmentView) {
        this.seleccionarMetodoPagoFragmentView = seleccionarMetodoPagoFragmentView;
        this.seleccionarMetodoPagoFragmentInteractorImpl = new SeleccionarMetodoPagoFragmentInteractorImpl(context);
    }

    @Override
    public void getPaymentMethodsFromServer(String publicKey, String amount){
        seleccionarMetodoPagoFragmentView.showProgress();
        this.seleccionarMetodoPagoFragmentInteractorImpl.getPaymentMethods(publicKey).subscribeWith(this.seleccionarMetodoPagoFragmentInteractorImpl.getObserverPaymentMethods(amount,new ResultListener<List<PaymentMethod>>() {
            @Override
            public void onSuccess(List<PaymentMethod> paymentMethodList) {
                seleccionarMetodoPagoFragmentView.hideProgress();
                seleccionarMetodoPagoFragmentView.showResultPaymentMethods(paymentMethodList);
            }

            @Override
            public void onError(String error) {
                seleccionarMetodoPagoFragmentView.hideProgress();
                seleccionarMetodoPagoFragmentView.showErrorPaymentMethods(error);
            }
        }));
    }



}
