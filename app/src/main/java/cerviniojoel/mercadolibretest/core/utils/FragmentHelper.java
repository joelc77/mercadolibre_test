package cerviniojoel.mercadolibretest.core.utils;

import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import cerviniojoel.mercadolibretest.R;

public class FragmentHelper {
    private static final String LOG_TAG = FragmentHelper.class.getSimpleName();

    public static void replaceFragment(FragmentManager fm, Fragment fragment, int idRes, boolean isBack) {
        try {
            String tag = fragment.getClass().getSimpleName();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();

            animarFragmento(isBack, fragmentTransaction);

            fragmentTransaction.add(idRes, fragment, tag);
            fragmentTransaction.addToBackStack(tag);
            fragmentTransaction.commit();
//            actual = (LoginBaseFragment) fragment;
        } catch (Exception e) {
            Log.e(LOG_TAG, "Exception en replaceFragment: " + e.getMessage());
        }
    }

    private static void animarFragmento(boolean isBack, FragmentTransaction fragmentTransaction) {
        if (isBack) {
            fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        } else {
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

    public static boolean replaceBackFragment(FragmentManager fm, int idRes) {
        try {
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            if(fm.getBackStackEntryCount()!=1) {
                animarFragmento(true, fragmentTransaction);
                Fragment fragment = getCurrentFragment(fm);
                fragmentTransaction.remove(fragment);
                fragmentTransaction.commit();
            }
            else {
                return false;
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "Exception en replaceFragment: " + e.getMessage());
            return false;
        }
        return true;
    }

    private static Fragment getBackStackFragment(FragmentManager fm, int index){
        String fragmentTag = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - index).getName();
        Fragment currentFragment = fm.findFragmentByTag(fragmentTag);
        return currentFragment;
    }

    private static Fragment getCurrentFragment(FragmentManager fm){
        Fragment fragment = getBackStackFragment(fm, 1);
        return fragment;
    }

    public static void removeFragments(FragmentManager fm){
        if(fm.getBackStackEntryCount() > 0){
            for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
//                if(i >0 ) {
                    fm.popBackStack();
//                }
            }
        }
    }

    public static void showDialogFragment(FragmentManager fm, DialogFragment dialogFragment){
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        dialogFragment.show(fragmentTransaction, "dialog");
    }
}
